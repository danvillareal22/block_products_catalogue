<?php
/**
 * block products catalogue
 *
 * @package    block_products_catalogue
 * @copyright  2017 SEBALE (http://sebale.net)
 */

require_once($CFG->dirroot . '/local/ecommerce/locallib.php');

class block_products_catalogue extends block_base {

    public $itemsperpage = 12;

    public function init() {
        $this->title = get_string('pluginname', 'block_products_catalogue');
    }

    /**
     * Return contents of course_catalogue block
     *
     * @return stdClass contents of block
     */
    public function get_content() {
        global $USER, $CFG, $DB, $SITE;

        if ($this->content !== null) {
            return $this->content;
        }

        if (!local_ecommerce_enable('', true)) {
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->text = '';
        $this->content->footer = '';

        $this->page->requires->css(new moodle_url($CFG->wwwroot . '/local/ecommerce/assets/css/bootstrap-select.min.css'));
        $search = optional_param('search', '', PARAM_TEXT);
        $currentCategory = optional_param('category', '', PARAM_INT);
        $orderBy = optional_param('orderby', '', PARAM_TEXT);
        if (empty($orderBy)) {
            $orderBy = 'featured';
        }

        $renderer = $this->page->get_renderer('local_ecommerce');
        $itemPerPage = get_config('local_ecommerce', 'default_number_products_per_page');
        $page = 0;

        $categories = \local_ecommerce\category::get_available_categories();
        $slider = \local_ecommerce\slider::get_slider();

        $this->content->text .=  $renderer->store_print_nav($categories, $search, $currentCategory, $orderBy, $slider);
        if (get_config('local_ecommerce', 'display_virtual_products')) {
            $virtualCategoryName = get_config('local_ecommerce', 'virtual_products_name');
            $virtualProducts = \local_ecommerce\store::get_store_products(0, $search, $orderBy, $itemPerPage, $page, $currentCategory);
            $this->content->text .= $renderer->store_print_products($itemPerPage, $virtualCategoryName, $virtualProducts, $page, 0);
        }
        if (get_config('local_ecommerce', 'display_physical_products')) {
            $physicalCategoryName = get_config('local_ecommerce', 'physical_products_name');
            $physicalProducts = \local_ecommerce\store::get_store_products(1, $search, $orderBy, $itemPerPage, $page, $currentCategory);
            $this->content->text .= $renderer->store_print_products($itemPerPage, $physicalCategoryName, $physicalProducts, $page, 1);
        }

        $this->content->text .=  $renderer->store_print_checkout_footer();

        $this->page->requires->js_call_amd('local_ecommerce/store', 'initSlider', array($slider->enableSlider));
        $this->page->requires->js_call_amd('local_ecommerce/ecommerce', 'purchased');
        $this->page->requires->js(new moodle_url("https://platform.linkedin.com/in.js"));

        /*$page = 0;

        $renderer = $this->page->get_renderer('block_products_catalogue');
        $categories = \local_ecommerce\category::get_available_categories($this->itemsperpage, $page);

        $this->content->text .= $renderer->products_catalogue($categories, $this->itemsperpage, $page);*/

        return $this->content;
    }

    /**
     * Allow the block to have a configuration page
     *
     * @return boolean
     */
    public function has_config() {
        return true;
    }

    /**
     * Locations where block can be displayed
     *
     * @return array
     */
    public function applicable_formats() {
        return array('all' => true);
    }

    function instance_allow_multiple() {
        return false;
    }

    function instance_create() {
        //global $PAGE;
        //$PAGE->set_context(context_course::instance(SITEID));
        return true;
    }

    /**
     * Sets block header to be hidden or visible
     *
     * @return bool if true then header will be visible.
     */
    public function hide_header() {
        return false;
    }
}