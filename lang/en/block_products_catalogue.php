<?php


$string['pluginname'] = 'Products Catalogue';

$string['dashboard'] = 'Dashboard';
$string['productscatalogue'] = 'Products Catalogue';
$string['category'] = 'Category: ';
$string['name'] = 'Name';
$string['loadmore'] = 'Load More';
$string['nocourses'] = 'No Products';
$string['allcategories'] = 'All Categories';
$string['showall'] = 'Show All';
$string['products_catalogue:myaddinstance'] = 'Add Products Catalogue block';
$string['products_catalogue:addinstance'] = 'Add Products Catalogue block';
$string['viewproduct'] = 'Click to view Product';
$string['noproducts'] = 'No Products';
$string['viewproducts'] = 'View Products';
$string['back'] = 'Back';
$string['details'] = 'Details';
$string['price'] = 'Price';
$string['code'] = 'Code';
$string['poweredby'] = 'Powered by <a href="https://intelliboard.net/" target="_blank">IntelliBoard</a>';
$string['incart'] = 'Added to cart';
$string['addtocart'] = 'Add to cart';
$string['inwaitlist'] = 'Added to waitlist';
$string['addtowaitlist'] = 'Add to waitlist';



