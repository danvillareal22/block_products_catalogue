<?php

namespace block_products_catalogue\output;

use plugin_renderer_base;
use renderable;
use context_system;
use html_writer;


/**
 * products catalogue block rendrer
 *
 * @package    block_products_catalogue
 * @copyright  2018 SEBALE (http://sebale.net)
 */
defined('MOODLE_INTERNAL') || die;

class renderer extends plugin_renderer_base {

    public $itemsperpage = 12;

    public function products_catalogue($categories = array(), $itemsperpage, $page) {
        global $USER, $OUTPUT, $CFG, $DB, $PAGE;

        $html = '';

        if (!count($categories['categories'])) {
            return $html;
        }

        $context = [
                'categories' => $categories['categories'],
                'iscategories' => count($categories['categories']),
                'showloadmore' => ($categories['total'] > $itemsperpage * ($page + 1)),
                'nextpage' => ($page+1),
                'shownoproducts' => (!count($categories['categories']) and $page > 0),
        ];

        $html = $this->render_from_template('block_products_catalogue/products_catalogue', $context);
        $this->page->requires->js_call_amd('block_products_catalogue/productscatalogue', 'init', array());
        $this->page->requires->js_call_amd('local_ecommerce/ecommerce', 'init', array());

        return $html;
    }

    public function catalogue_print_products($category = null, $products = array(), $itemsperpage, $page = 0) {

        $categorydescription = \local_ecommerce\category::get_category_info($category);

        $context = [
                'products' => $products['products'],
                'isproducts' => count($products['products']),
                'showloadmore' => ($products['total'] > $itemsperpage * ($page + 1)),
                'nextpage' => ($page+1),
                'shownoproducts' => (!count($products['products']) and $page > 0),
                'showcategorydescription' => (!$page and $category->id),
                'categoryname' => $category->name,
                'categorydescription' => $categorydescription,
                'cancheckout' => has_capability('local/ecommerce:checkout', context_system::instance()),
                'currency' => \local_ecommerce\payment::get_currency(),
                'enablewaitlist' => get_config('local_ecommerce', 'enablewaitlist'),
        ];

        return $this->render_from_template('block_products_catalogue/products_list', $context);
    }
}
