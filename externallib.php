<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * block_products_catalogue API
 *
 * @package    block_products_catalogue
 * @category   external
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("$CFG->libdir/externallib.php");

/**
 * Message external functions
 *
 * @package    block_products_catalogue
 * @category   external
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_products_catalogue_external extends external_api {

    /**
     * Load categories
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function load_categories_parameters() {
        return new external_function_parameters(
            array(
                'page' => new external_value(PARAM_INT, 'the page number'),
                'search' => new external_value(PARAM_TEXT, 'search categories')
            )
        );
    }

    /**
     * Get categories function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  int      $page               the page number
     * @param  char     $search             search categories
     * @return external_categories, external_total
     */
    public static function load_categories($page, $search) {
        global $USER, $PAGE, $CFG;

        $params = self::validate_parameters(
            self::load_categories_parameters(),
            array(
                'page' => $page,
                'search' => $search,
            )
        );

        $PAGE->set_context(context_system::instance());

        $page = $params['page'];
        $search = $params['search'];

        $renderer = $PAGE->get_renderer('block_products_catalogue');
        $categories = \local_ecommerce\category::get_available_categories($renderer->itemsperpage, $page);

        $context = [
                'categories' => $categories['categories'],
                'iscategories' => count($categories['categories']),
                'showloadmore' => ($categories['total'] > $renderer->itemsperpage * ($page + 1)),
                'nextpage' => ($page+1),
                'shownoproducts' => (!count($categories['categories']) and $page > 0),
        ];

        $output = $renderer->render_from_template('block_products_catalogue/categories_list', $context);

        return array('categories'=>$output);
    }

    /**
     * Get and return categories.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function load_categories_returns() {
        return new external_single_structure(
            array(
                'categories' => new external_value(PARAM_RAW, 'categories html'),
            )
        );
    }

    /**
     * Load products
     *
     * @return external_function_parameters
     * @since 3.2
     */
    public static function load_products_parameters() {
        return new external_function_parameters(
            array(
                'page' => new external_value(PARAM_INT, 'the page number'),
                'category' => new external_value(PARAM_INT, 'the category id number'),
                'search' => new external_value(PARAM_TEXT, 'search products')
            )
        );
    }

    /**
     * Get announcements function.
     *
     * @since  3.2
     * @throws invalid_parameter_exception
     * @throws moodle_exception
     * @param  int      $page               the page number
     * @param  int      $category           the page number
     * @param  char     $level              the level number
     * @param  char     $search             search products
     * @return external_products, external_total
     */
    public static function load_products($page, $category, $search) {
        global $USER, $PAGE, $CFG, $DB;

        $params = self::validate_parameters(
            self::load_products_parameters(),
            array(
                'page' => $page,
                'category' => $category,
                'search' => $search,
            )
        );

        $PAGE->set_context(context_system::instance());

        $page = $params['page'];
        $category = $params['category'];
        $search = $params['search'];

        $category = $DB->get_record('local_ecommerce_cat', array('id'=>$category));

        $renderer = $PAGE->get_renderer('block_products_catalogue');
        $products = \local_ecommerce\product::get_available_products($renderer->itemsperpage, $page, $category->id);

        $output = $renderer->catalogue_print_products($category, $products, $renderer->itemsperpage, $page);

        return array('products'=>$output);
    }

    /**
     * Get and return products.
     *
     * @return external_single_structure
     * @since 3.2
     */
    public static function load_products_returns() {
        return new external_single_structure(
                array(
                        'products' => new external_value(PARAM_RAW, 'products html'),
                )
        );
    }
}
