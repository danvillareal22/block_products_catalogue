// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    block_products_catalogue
 * @copyright  2017 SEBALE (http://sebale.net)
 */

define(['jquery', 'core/ajax', 'core/str', 'core/config', 'core/log'], function($, ajax, str, mdlcfg, log) {

    var Catalogue = {

        loader: '<div class="loader"><i class="fa fa-circle-o-notch fa-spin"></i></div>',
        contentcontainer: '#availableproducts',

        init: function() {

            container = $(Catalogue.contentcontainer);

            container.on('click', '#loadmore-categories', function(event) {
                event.preventDefault();
                var page = $(this).data('page');
                Catalogue.loadCategories(0);
            });

            container.on('click', '.back-button-box', function(event) {
                event.preventDefault();
                Catalogue.loadCategories(0);
            });

            container.on('click', '#loadmore-products', function(event) {
                event.preventDefault();
                var page = $(this).data('page');
                var category = $(this).data('categoryid');
                Catalogue.loadProducts(category, page);
            });

            container.on('click', '.category-view-btn', function(event) {
                event.preventDefault();
                var category = $(this).parents('.categorybox').data('categoryid');
                Catalogue.loadProducts(category, 0);
            });

        },

        loadProducts: function (category, page) {

            container = $(Catalogue.contentcontainer);

            if (container.length) {
                container.find('.load-more-box').remove();
                container.find('.products').append(Catalogue.loader);
                container.find('.products').addClass('loading');

                ajax.call([{
                    methodname: 'block_products_catalogue_load_products', args: {
                        category: category,
                        page: page,
                        search: ''
                    }
                }])[0]
                    .done(function (response) {
                        try {
                            container.find('.back-button-box').removeClass('hidden');
                            container.find('.products').html(response.products).removeClass('loading');
                            container.find('.loader').remove();
                        } catch (Error) {
                            log.debug(Error.message);
                            container.find('.products').html(response.products).removeClass('loading');
                            container.find('.loader').remove();
                        }
                    }).fail(function (ex) {
                    log.debug(ex.message);
                    container.find('.products').html(response.products).removeClass('loading');
                    container.find('.loader').remove();
                });
            }

        },

        loadCategories: function (page) {

            container = $(Catalogue.contentcontainer);

            if (container.length) {
                container.find('.load-more-box').remove();
                container.find('.products').append(Catalogue.loader);
                container.find('.products').addClass('loading');

                ajax.call([{
                    methodname: 'block_products_catalogue_load_categories', args: {
                        page: page,
                        search: ''
                    }
                }])[0]
                    .done(function (response) {
                        try {
                            container.find('.back-button-box').addClass('hidden');
                            container.find('.products').html(response.categories).removeClass('loading');
                            container.find('.loader').remove();
                        } catch (Error) {
                            log.debug(Error.message);
                            container.find('.products').html(response.categories).removeClass('loading');
                            container.find('.loader').remove();
                        }
                    }).fail(function (ex) {
                    log.debug(ex.message);
                    container.find('.products').html(response.categories).removeClass('loading');
                    container.find('.loader').remove();
                });
            }

        },

    };

    /**
     * @alias module:block_products_catalogue/productscatalogue
     */
    return Catalogue;

});