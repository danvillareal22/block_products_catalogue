<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * External functions and service definitions.
 *
 * @package    block_products_catalogue
 * @copyright  2018 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$functions = array(
        'block_products_catalogue_load_categories' => array(
                'classname' => 'block_products_catalogue_external',
                'methodname' => 'load_categories',
                'classpath' => 'blocks/products_catalogue/externallib.php',
                'description' => 'Retrieve a list of ecommerce categories',
                'type' => 'read',
                'ajax' => true,
                'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
                'loginrequired' => false
        ),
        'block_products_catalogue_load_products' => array(
                'classname' => 'block_products_catalogue_external',
                'methodname' => 'load_products',
                'classpath' => 'blocks/products_catalogue/externallib.php',
                'description' => 'Retrieve a list of ecommerce products by category',
                'type' => 'read',
                'ajax' => true,
                'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
                'loginrequired' => false
        ),
);
