<?php
/**
 * course catalogue
 *
 * @package    block_course_catalogue
 * @copyright  2015 SEBALE (http://sebale.net)
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    'block/products_catalogue:myaddinstance' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'user' => CAP_ALLOW
        ),

        'clonepermissionsfrom' => 'moodle/site:config'
    ),

    'block/products_catalogue:addinstance' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,

        'captype' => 'write',
        'contextlevel' => CONTEXT_BLOCK,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        ),

        'clonepermissionsfrom' => 'moodle/site:config'
    )
);
